import React from 'react'
import posed from 'react-pose'

const ProgressFiller = posed.div({
  empty: {
    width: 0,
  },
  full: {
    width: ({ value }) => {
      return value + '%'
    },
    transition: { duration: 3000 },
  },
  props: { value: 0 },
})

class SkillsProgress extends React.Component {
  constructor() {
    super()
    this.state = { isReady: false }
  }

  componentDidMount() {
    this.setState({ isReady: true })
  }

  render() {
    return (
      <div style={{ width: '100%' }} className="margin-size">
        <div
          className="container"
          style={{ display: 'flex', borderRadius: '12px' }}
        >
          <ProgressFiller
            className="skills"
            style={{
              backgroundColor: this.props.color,
              display: 'inline-block',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              borderRadius: '12px',
            }}
            pose={this.state.isReady ? 'full' : 'empty'}
            value={this.props.progress}
          >
            <div className="subtext-progress">{this.props.subtext}</div>
            <div style={{ float: 'right' }}>{this.props.text}</div>
          </ProgressFiller>
        </div>
      </div>
    )
  }
}

class VisserProgress extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="" style={{ marginRight: '0.5rem' }}>
        <SkillsProgress
          progress="100"
          text="UX Engineering"
          subtext="Expert"
          color="#223B73"
        />
        <SkillsProgress
          progress="95"
          text="Android"
          subtext="Pro"
          color="#771326"
        />
        <SkillsProgress
          progress="90"
          text="Java"
          subtext="Highly experienced"
          color="#233E3F"
        />
        <SkillsProgress
          progress="85"
          text="Javascript"
          subtext="Real good"
          color="#AAC89E"
        />
        <SkillsProgress
          progress="70"
          subtext="Quite proficient"
          text="CI / CD"
          color="#3365CD"
        />
        <SkillsProgress
          progress="65"
          subtext="Proficient"
          text="Reactive Programming"
          color="#5C5C60"
        />
        <SkillsProgress
          progress="62"
          subtext="Successful"
          text="Project Management"
          color="#DB2D16"
        />
        <SkillsProgress
          progress="60"
          text="React"
          subtext="Recently started"
          color="#C7898B"
        />
        <SkillsProgress
          progress="40"
          text="Spring"
          subtext="Want to learn"
          color="#C24AE4"
        />
      </div>
    )
  }
}

export default VisserProgress
