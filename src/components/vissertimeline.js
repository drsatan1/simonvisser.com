import React from 'react'

import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component'
import 'react-vertical-timeline-component/style.min.css'

const TimelineElement = ({
  date,
  lineOne,
  lineTwo,
  lineThree,
  lineFour,
  iconName,
  color,
}) => (
  <VerticalTimelineElement
    className="vertical-timeline-element--work"
    date={date}
    iconStyle={{ background: color, color: '#fff' }}
    icon={<i style={{ width: '24px' }} className={'is-large ' + iconName} />}
  >
    <h3 className="vertical-timeline-element-title is-size-5">{lineOne}</h3>
    <h4 className="vertical-timeline-element-subtitle has-text-weight-light">
      {lineTwo}
    </h4>
    <p>{lineThree}</p>
    <p className="has-text-weight-light">{lineFour}</p>
  </VerticalTimelineElement>
)

function VisserTimeline() {
  return (
    <VerticalTimeline>
      <TimelineElement
        date="2018-Present"
        lineOne="Gotbot"
        lineTwo="Cape Town"
        lineThree="CI/CD, Reporting, Node, AWS, Angular"
        lineFour="With Gotbot, I've been given the opportunity to be part of a quickly-growing business with a strong connection to ML, and a need to scale rapidly on the cloud. All modern tech is fair game."
        iconName="fa-robot fas"
        color="#cc0000"
      />
      <TimelineElement
        date="2017-2018"
        lineOne="Discovery"
        lineTwo="APS Mobile"
        lineThree="Agile, Spring, Native Android, Image Processing, Animation, Remote Work"
        lineFour="
        As part of Discovery APS' remote squad in Cape Town, I've learnt to
        successfully navigate corporate corridors, as well as be effective across
        horizontal slices of the business."
        iconName="fa-mobile-alt fas"
        color="#1313c4"
      />
      <TimelineElement
        date="2017-2018"
        lineOne="Entelect"
        lineTwo="Cape Town"
        lineThree="Contractor, Software Engineer, Team Member, Board Games Club"
        lineFour="Being involved with Entelect has made me both appreciate the importance of the culture around software engineering, and help my find my own place in it."
        iconName="fa-edge fab"
        color="#6a00ff"
      />
      <TimelineElement
        date="2016-2017"
        lineOne="Flash Mobile Vending"
        lineTwo="Cape Town"
        lineThree="Android, Ionic, Angular, Node, Embedded, CI and CD"
        lineFour="Flash Mobile Vending is a business with strong ties to the grass roots of South Africa. My duties at Flash included sole responsibility of a succesful hybrid application as well as packaging and shipping SDK's for bespoke device hardware."
        iconName="fab fa-angular"
        color="#35f13b"
      />
      <TimelineElement
        date="2012-2016"
        lineOne="Bsc Computer Science"
        lineTwo="University of Johannesburg"
        lineThree="Computer Science, Informatics, Mathematics, Business Management"
        lineFour="University finely honed my craft, and brought me to superhuman heights, the likes of which only previously achieved by Gods and demons."
        iconName="fas fa-graduation-cap"
        color="#bedf31"
      />
      <TimelineElement
        date="2014-2015"
        lineOne="ASD.co.za"
        lineTwo="Johannesburg South"
        lineThree="Java EE, Enterprise System, KM, Work While Studying"
        lineFour="My experience working with a small developer house who preferred to hire non-graduates for a cut rate and teach them an in-house brand of programming framework made me realise how badly I need to finish university."
        iconName="fab fa-java"
        color="#db23c2"
      />
      <TimelineElement
        date="2011"
        lineOne="Matriculation"
        lineTwo="Linden Hoërskool"
        lineThree="Tennis, Cricket, Distinctions in Afrikaans, Mathematics"
        lineFour="I was once a child, it is true, though some say that I emerged from the womb a bearded baby."
        iconName="fas fa-school"
        color="#00fff2"
      />
    </VerticalTimeline>
  )
}
export default VisserTimeline
