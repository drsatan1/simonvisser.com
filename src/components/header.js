import React from 'react'
import Link from 'gatsby-link'

import posed from 'react-pose'

import styles from '../layouts/index.scss'

const BurgerMenuPose = posed.div({
  visible: {
    delayChildren: 200,
    staggerChildren: 50,
  },
  hidden: { delay: 300 },
})

const BurgerMenuItem = posed.div({
  visible: { y: 0, opacity: 1 },
  hidden: { y: -20, opacity: 0 },
})

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isToggleOn: false, windowWidth: 'undefined' }

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn,
    }))
  }

  handleResize = () => {
    this.setState({
      windowWidth: window.innerWidth,
    })
  }

  componentDidMount() {
    this.handleResize()
    window.addEventListener('resize', this.handleResize)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  render() {
    return (
      <nav className="navbar is-transparent" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <div className="margin-size">
            <h1 className="is-size-4">
              <Link to="/">{this.props.siteTitle}</Link>
            </h1>
          </div>

          <a
            role="button"
            className={
              'navbar-burger ' + (this.state.isToggleOn ? 'is-active' : '')
            }
            aria-label="menu"
            aria-expanded="false"
            onClick={this.handleClick}
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>
        <BurgerMenuPose
          pose={
            this.state.isToggleOn ||
            this.state.windowWidth > parseInt(/*styles.desktopWidth*/ 1088, 10)
              ? 'visible'
              : 'hidden'
          }
        >
          <div
          style={{}}
            className={
              'navbar-menu ' +
              (this.state.isToggleOn ? 'is-active' : '')
            }
          >
            <div className="navbar-start">
              <BurgerMenuItem>
                <Link className="navbar-item" to="/about/">
                  About
                </Link>
              </BurgerMenuItem>
              <BurgerMenuItem>
                <Link className="navbar-item" to="/blog/">
                  Blog
                </Link>
              </BurgerMenuItem>
              <BurgerMenuItem>
                <Link className="navbar-item" to="/contact/">
                  Contact
                </Link>
              </BurgerMenuItem>
              <BurgerMenuItem>
                <Link className="navbar-item" to="/nodes/">
                  Nodes
                </Link>
              </BurgerMenuItem>
            </div>
            <div className="navbar-end" />
          </div>
        </BurgerMenuPose>
      </nav>
    )
  }
}

export default Header
