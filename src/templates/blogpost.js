import React from 'react'

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <div
      className="content margin-size"
      style={{ maxWidth: '60vw', marginLeft: 'auto', marginRight: 'auto' }}
    >
      <section class="hero is-primary" style={{marginBottom: '0.5rem'}}>
        <div class="hero-body">
          <div class="">
            <h1 class="title">{post.frontmatter.title}</h1>
          </div>
        </div>
      </section>
      <div dangerouslySetInnerHTML={{ __html: post.html }} />
    </div>
  )
}

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`
