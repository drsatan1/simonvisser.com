import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Header from '../components/header'
import './index.scss'

const Layout = ({ children, data }) => (
  <div className="aboveWrapper">
    <div id="wrapper">
      <Helmet
        title={data.site.siteMetadata.title}
        meta={[
          { name: 'description', content: 'Sample' },
          { name: 'keywords', content: 'sample, something' },
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        ]}
      >
        <script
          defer
          src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"
        />
      </Helmet>

      <Header siteTitle={data.site.siteMetadata.title} />
      <div style={{ display: 'flex', flex: 1, flexDirection: 'column' }}>{children()}</div>
    </div>
    <footer className="footer has-background-light">
      <div className="content has-text-centered ">
        <p>
          <strong>simonvisser.com</strong> by{' '}
          <a href="https://simonvisser.com">Simon Visser</a>. <br />
          using <a href="https://www.gatsbyjs.org/Built ">Gatsby</a>,
          <a href="https://www.netlify.com ">Netlify </a>,{' '}
          <a href="https://reactjs.org/ ">React </a>
        </p>
      </div>
    </footer>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
