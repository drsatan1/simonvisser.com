import React from 'react'
import Link from 'gatsby-link'

class NodeSidebar extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <aside className="menu" style={{ listStyle: 'none' }}>
        <a
          onClick={() => {
            this.props.addChildNode(null)
          }}
          className="button is-normal is-info margin-size"
        >
          <span className="icon is-small">
            <i className="fas fa-plus" />
          </span>
          <span>Add root node</span>
        </a>

        <a
          onClick={() => {
            this.props.toggleJsonInput()
          }}
          className="button is-normal is-success margin-size"
        >
          <span className="icon is-small">
            <i className="fas fa-code" />
          </span>
          <span>From json</span>
        </a>

        <ul className="menu-list">
          {Object.keys(this.props.nodes).map(
            key =>
              this.props.nodes[key].parent == 0
                ? this.displayNodeRecursive(key)
                : ''
          )}
        </ul>
      </aside>
    )
  }

  displayNodeRecursive(key) {
    return (
      <li>
        <a
          className={
            this.props.nodes[key] == this.props.selectedNode ? 'is-active' : ''
          }
          onClick={() => this.props.onNodeSelected(this.props.nodes[key])}
        >
          {this.props.nodes[key].text}
        </a>
        <ul>
          {this.findChildren(key).map(childKey =>
            this.displayNodeRecursive(childKey)
          )}
        </ul>
      </li>
    )
  }

  findChildren(key) {
    return Object.keys(this.props.nodes).filter(filterKey => {
      return this.props.nodes[filterKey].parent == key
    })
  }
}

class NodeDisplay extends React.Component {
  render() {
    return (
      <div>
        <div
          className={!this.props.selectedNode ? 'hidden' : ''}
          style={{ paddingBottom: '2px' }}
        >
          <p className="buttons">
            <a
              onClick={() => this.props.addChildNode(this.props.selectedNode)}
              className="button is-info is-outlined"
            >
              <span>Add child node</span>
              <span className="icon is-small">
                <i className="fas fa-plus" />
              </span>
            </a>
            <a
              onClick={() => {
                this.props.deleteNode(this.props.selectedNode)
              }}
              className="button is-danger is-outlined"
            >
              <span>Delete this node</span>
              <span className="icon is-small">
                <i className="fas fa-times" />
              </span>
            </a>
          </p>
        </div>
        <div>
          <p>
            {this.props.selectedNode
              ? this.props.selectedNode.content
              : 'Select a node!'}
          </p>
        </div>
      </div>
    )
  }
}

class NodesModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      titleValue: '',
      textValue: '',
    }

    this.handleTitle = this.handleTitle.bind(this)
    this.handleText = this.handleText.bind(this)
  }

  handleTitle(event) {
    this.setState({ titleValue: event.target.value })
  }

  handleText(event) {
    this.setState({ textValue: event.target.value })
  }

  render() {
    return (
      <div className={'modal' + (this.props.isVisible ? ' is-active' : '')}>
        <div className="modal-background" />
        <div className="modal-content">
          <div className="card">
            <div className="card-content">
              <div className="content">
                <div className="field">
                  <label className="label">Title</label>
                  <div className="control">
                    <input
                      onChange={this.handleTitle}
                      value={this.state.titleValue}
                      className="input"
                      type="text"
                      placeholder="The node's title..."
                    />
                  </div>
                </div>
                <div className="field">
                  <label className="label">Text</label>
                  <div className="control">
                    <input
                      onChange={this.handleText}
                      value={this.state.textValue}
                      className="input"
                      type="text"
                      placeholder="This node's content..."
                    />
                  </div>
                </div>
              </div>
            </div>
            <footer className="card-footer">
              <a
                onClick={() => {
                  this.setState({ titleValue: '', textValue: '' })
                  this.props.toggleModal()
                  this.props.addChildNode(
                    this.props.selectedKey,
                    this.state.titleValue,
                    this.state.textValue
                  )
                }}
                href="#"
                className="card-footer-item"
              >
                Save
              </a>
            </footer>
          </div>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={this.props.toggleModal}
        />
      </div>
    )
  }
}

class JsonModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      jsonValue: '',
    }

    this.handleJson = this.handleJson.bind(this)
  }

  handleJson(event) {
    this.setState({ jsonValue: event.target.value })
  }

  render() {
    return (
      <div className={'modal' + (this.props.isVisible ? ' is-active' : '')}>
        <div className="modal-background" />
        <div className="modal-content">
          <div className="card">
            <div className="card-content">
              <div className="content">
                <div className="field">
                  <div class="notification is-danger">
                    Adding your nodes as json will erase existing nodes! <br />
                    This method of entering input doesn't support node contents.
                  </div>
                  <div class="notification is-info">
                    Here's an example value: <br />
                    {
                      '[ {"id": 1, "label": "2", "parentID": 0}, {"id": 2, "label": "3", "parentID": 1} ]'
                    }
                  </div>
                  <div className="control">
                    <textarea
                      class="textarea"
                      onChange={this.handleJson}
                      value={this.state.jsonValue}
                      placeholder="Your nodes as json"
                    />
                  </div>
                </div>
              </div>
            </div>
            <footer className="card-footer">
              <a
                onClick={() => {
                  this.setState({ jsonValue: '' })
                  this.props.toggleModal()
                  this.props.addNodesAsJson(this.state.jsonValue)
                }}
                href="#"
                className="card-footer-item"
              >
                Save
              </a>
            </footer>
          </div>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={this.props.toggleModal}
        />
      </div>
    )
  }
}

class NodesPage extends React.Component {
  constructor(props) {
    super(props)
    this.nodeSelected = this.nodeSelected.bind(this)
    this.addChildNode = this.addChildNode.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.deleteNode = this.deleteNode.bind(this)
    this.toggleJsonInput = this.toggleJsonInput.bind(this)
    this.addNodesAsJson = this.addNodesAsJson.bind(this)
    this.state = {
      nodes: {
        '1': {
          parent: 0,
          text: 'Example root node',
          content:
            'You can have more than one root node! Try it out by clicking "add node" to add a root node, or "add child node" to add a node as a child of your selected node.',
        },
      },
      selectedNode: null,
      modalVisible: false,
      jsonModalVisible: false,
    }
  }

  addNodesAsJson(jsonValues) {
    console.log(jsonValues)
    var json = JSON.parse(jsonValues)
    this.setState({ nodes: {} }, () => {
      json.forEach(element => {
        this.addNodeNaive(element.parentID, element.label, element.id)
      })
    })
  }

  toggleJsonInput() {
    this.setState({ jsonModalVisible: !this.state.jsonModalVisible })
  }

  deleteNode(node) {
    if (this.state.nodes && node) {
      var newNodes = this.state.nodes
      var key = Object.keys(newNodes).find(key => newNodes[key] === node)
      Object.keys(newNodes).map(element => {
        if (element.parent == key) {
          deleteNode(element)
        }
      })
      delete newNodes[key]

      this.setState({ selectedNode: null, nodes: newNodes })
    }
  }

  nodeSelected(node) {
    this.setState({ selectedNode: node })
  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8
      return v.toString(16)
    })
  }

  addNodeNaive(parentKey, title, id) {
    var newNodes = this.state.nodes
    newNodes[id] = {
      parent: parentKey,
      text: title,
      content: '',
    }

    this.setState({ nodes: newNodes })
  }

  addChildNode(parentKey, title, text) {
    var newNodes = this.state.nodes
    newNodes[this.uuidv4()] = {
      parent: parentKey,
      text: title,
      content: text,
    }
    this.setState({
      nodes: newNodes,
    })
  }

  toggleModal(node) {
    var key
    if (node == null) {
      key = 0
    } else {
      key = Object.keys(this.state.nodes).find(
        key => this.state.nodes[key] === node
      )
    }
    this.setState({ selectedKey: key, modalVisible: !this.state.modalVisible })
  }

  render() {
    return (
      <div id="nodesPage" className="has-text-black">
        <JsonModal
          isVisible={this.state.jsonModalVisible}
          toggleModal={this.toggleJsonInput}
          addNodesAsJson={this.addNodesAsJson}
        />

        <NodesModal
          selectedKey={this.state.selectedKey}
          addChildNode={this.addChildNode}
          isVisible={this.state.modalVisible}
          toggleModal={this.toggleModal}
        />
        <div
          style={{
            maxWidth: '1080px',
            marginTop: '1rem',
            marginLeft: 'auto',
            marginRight: 'auto',
          }}
          className="columns"
        >
          <div className="column is-one-third">
            <NodeSidebar
              nodes={this.state.nodes}
              selectedNode={this.state.selectedNode}
              onNodeSelected={this.nodeSelected}
              addChildNode={this.toggleModal}
              toggleJsonInput={this.toggleJsonInput}
            />
          </div>
          <div className="column is-two-thirds">
            <div className="content box">
              <NodeDisplay
                deleteNode={this.deleteNode}
                addChildNode={this.toggleModal}
                selectedNode={this.state.selectedNode}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default NodesPage
