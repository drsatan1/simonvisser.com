import React from 'react'
import { navigateTo } from 'gatsby-link'

function encode(data) {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

export default class Contact extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleSubmit = e => {
    e.preventDefault()
    const form = e.target
    fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: encode({
        'form-name': form.getAttribute('name'),
        ...this.state,
      }),
    })
      .then(
        /*() => navigateTo(form.getAttribute('action'))*/ () => {
          window.alert("Thanks! I'll get back to you soon.")
        }
      )
      .catch(error => alert(error))
  }

  render() {
    return (
      <section class="section" id="contactPage">
        <div class="container" style={{ padding: '0.5rem' }}>
          <div className="margin-size">
            <form
              name="contact"
              method="post"
              // action="/thanks/"
              data-netlify="true"
              data-netlify-honeypot="bot-field"
              onSubmit={this.handleSubmit}
            >
              {/* The `form-name` hidden field is required to support form submissions without JavaScript */}
              <input type="hidden" name="form-name" value="contact" />
              <p hidden>
                <label>
                  Don’t fill this out:{' '}
                  <input name="bot-field" onChange={this.handleChange} />
                </label>
              </p>
              <div class="field">
                <label class="label">Name</label>
                <div class="control">
                  <input
                    type="text"
                    name="name"
                    class="input"
                    onChange={this.handleChange}
                  />
                </div>
              </div>

              <div class="field">
                <label class="label">Email</label>
                <div class="control has-icons-left">
                  <input
                    class="input"
                    type="email"
                    name="email"
                    onChange={this.handleChange}
                  />
                  <span
                    class="icon is-small is-left"
                    style={{ left: '5px', top: '5px' }}
                  >
                    <i class="fas fa-envelope" />
                  </span>
                </div>
              </div>
              <div class="field">
                <label class="label">Message</label>
                <div class="control">
                  <textarea
                    class="textarea"
                    name="message"
                    onChange={this.handleChange}
                  />
                </div>
              </div>
              <div class="control">
                <button class="button is-link" type="submit">
                  Send
                </button>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}
