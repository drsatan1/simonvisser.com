import React from 'react'
import Link from 'gatsby-link'

class IndexPage extends React.Component {
  render() {
    return (
      <div id="indexPage" className="content has-text-white">
        <div
          style={{
            maxWidth: '800px',
            marginTop: '1rem',
            marginLeft: 'auto',
            marginRight: 'auto',
          }}
        >
          <h1 className="has-text-white">Hello, world</h1>
          <p>
            Hit me on the socials:
            <br /> 
            <a href="https://twitter.com/CannibalOfKush"><a class="fab fa-twitter social"></a></a>
            <a href="https://www.linkedin.com/in/simon-visser-70a4a498/"><a class="fab fa-linkedin social"></a></a>
            <br />
            And watch this space for ELKs
          </p>
        </div>
      </div>
    )
  }
}
export default IndexPage
