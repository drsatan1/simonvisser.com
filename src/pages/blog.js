import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'

export default ({ data }) => {
  return (
    <div className="">
      <div
        className="column is-light is-center"
        style={{ maxWidth: '800px', marginLeft: 'auto', marginRight: 'auto' }}
      >
        {data.allMarkdownRemark.edges.map(({ node }, index) => {
          return (
            <div key={node.id}>
              <article className="media margin-size">
                <figure className="media-left">
                  <div className="image is-256x256">
                    <Link to={node.fields.slug}>
                      <Img
                        style={{ borderRadius: '25%', float: 'left' }}
                        className="margin-size"
                        resolutions={
                          node.frontmatter.image.childImageSharp.resolutions
                        }
                      />
                    </Link>
                  </div>
                </figure>
                <div className="media-content">
                  <div className="content">
                    <p>
                      <br />
                      <Link to={node.fields.slug}>
                        <strong>{node.frontmatter.title}</strong>{' '}
                      </Link>{' '}
                      <br /> <br />
                      <small>{node.frontmatter.date} </small>
                      <br />
                      {node.excerpt}
                      <Link
                        to={node.fields.slug}
                        className="has-text-weight-light"
                      >
                        {' '}
                        (keep reading...)
                      </Link>
                    </p>
                  </div>
                  {/* <nav className="level is-mobile">
                  <div className="level-left">
                    <a className="level-item">
                      <span className="icon is-small">
                        <i className="fas fa-reply" />
                      </span>
                    </a>
                    <a className="level-item">
                      <span className="icon is-small">
                        <i className="fas fa-retweet" />
                      </span>
                    </a>
                    <a className="level-item">
                      <span className="icon is-small">
                        <i className="fas fa-heart" />
                      </span>
                    </a>
                  </div>
                </nav> */}
                </div>
              </article>{' '}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export const query = graphql`
  query IndexQuery {
    allMarkdownRemark {
      totalCount
      edges {
        node {
          fields {
            slug
          }
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            image {
              childImageSharp {
                resolutions(width: 225, height: 225) {
                  ...GatsbyImageSharpResolutions
                }
              }
            }
          }
          excerpt
        }
      }
    }
  }
`
