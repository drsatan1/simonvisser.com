import React from 'react'
import VisserTimeline from '../components/vissertimeline'
import VisserProgress from '../components/visserprogress'
import Img from 'gatsby-image'
import posed from 'react-pose'
import Contact from '../components/contact'

const FadeBox = posed.div({
  invisible: { opacity: 0 },
  visible: { opacity: 1, transition: { duration: 1500 } },
})

class AboutPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { isReady: false }
  }

  componentDidMount() {
    this.setState({ isReady: true })
  }

  render() {
    return (
      <div className="columns">
        <div className="column is-two-fifths">
          <Img
            style={{ borderRadius: '25%', float: 'left' }}
            className="margin-size"
            resolutions={this.props.data.file.childImageSharp.resolutions}
          />
          <FadeBox
            pose={this.state.isReady ? 'visible' : 'invisible'}
            className="margin-size"
          >
            <h2 className="is-size-5">Essential Roles</h2>
            <p className="has-text-weight-light">
              Engineering, Design, Project Management
            </p>
            <br />
            <p className="has-text-weight-light">
              Check out my{' '}
              <a href="https://www.linkedin.com/in/simon-visser-70a4a498/">
                {' '}
                LinkedIn{' '}
              </a>
              and <a href="https://github.com/drsatan1"> GitHub </a>
            </p>
          </FadeBox>

          <div className="margin-size">
            <div className="content">
              <h1>Welcome to my online CV</h1>
              <p className="has-text-weight-light">If you're here it's probably because I told you to go to my website in real life.</p>
              <p className="has-text-weight-light">For the rest of you, please take a moment to get a load of the nice animations around the page. The bars fill up, the bubbles fly in and the menu items fade in and out. Swanky.</p>
            </div>
          </div>

          <VisserProgress />

        </div>
        <div className="column is-light">
          <VisserTimeline />
        </div>
      </div>
    )
  }
}

export default AboutPage

export const query = graphql`
  query GatsbyImageSampleQuery {
    file(relativePath: { eq: "img/profile.png" }) {
      childImageSharp {
        # Specify the image processing specifications right in the query.
        # Makes it trivial to update as your page's design changes.
        resolutions(width: 100, height: 100) {
          ...GatsbyImageSharpResolutions
        }
      }
    }
  }
`
