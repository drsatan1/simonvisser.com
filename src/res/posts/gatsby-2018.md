---
title: 'Is Gatsby really that great?'
date: '2018-09-16'
image: '../img/gatsby.png'
---

### Building a static site in Gatsby is tougher than it looks, but it's worth the effort.

As you may already have guessed, this website is built using the fancy, new Gatsby static site generator. My choice of Gatsby was based mostly on whim, and is heavily guided by "flavour-of-the-month" consideration. You'll find [many developers](https://medium.com/@elinaschaefer/why-using-gatsbyjs-cb2f1cb1c586) singing the [praises](https://medium.com/front-end-hacking/first-impressions-gatsby-js-e7f9b3130fac) of this funky new framework, and for good reason.

Gatsby unifies the power of Facebook's React with modern web-centric technologies. Features such as image compression and thumbnailing, page generation through data from GraphQL queries at run-time, easy-mode plugins for connecting to your data sources, and a strict adherence to page speed principles all come native to the package; possibly earning this little guy a spot in modern serverless development for a while in the near future.

Compound to that the fact that version two of this box is nearly on the way, you can count me excited. But where does Gatsby go wrong?

Some issues I ran into personally were:

- Gatsby uses React@15. It's always nice to stay up to date with the greatest, as version 16 of React boasts [some nice improvements.](https://reactjs.org/blog/2017/09/26/react-v16.0.html) This will always be the symptom of playing "centipede" with a framework such as Gatsby, though.
- The framework is young, and it shows. I tried to use [GraphCMS] to store data for my site, just for fun if nothing else, but because the CMS doesn't correctly specify its markdown fields as "text\markdown", Gatsby's transformer plugin didn't detect it as markdown, and wouldn't correctly transform it to HTML for me. Bug report incoming.
- While extremely powerful, Gatsby's API's seem clunky and not pointed at a purpose. This is a double-edged sword for any API as pushing any one single function usually comes to the detriment of all others, but having more concise functions to transform data at build time would only serve as a positive for users, though this is heavily opinion-based.
- User-friendliness is not extremely high with Gatsby, at least not as high as other related tools, such as Wordpress. This is not necessarily the worst thing, as the target market for Gatsby is definitely more technically-focused, and doesn't sell itself as an easy "build your own site" tool.

However, with these teething pains in mind, the framework still blows me away. With minimal config, I'm able to set up and deploy a fully-featured serverless website. The JAM stack is quickly solidifying its place as the next big web development paradigm, and front-end developers are here to stay.
