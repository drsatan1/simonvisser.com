---
title: 'Trust should be assumed, not earned'
date: '2018-09-10'
image: '../img/trust.jpeg'
---

I recently had an interesting conversation with my manager regarding trust. A conversation that illuminated to me, at the time, an integral part of how other people think that may not necessarily correlate extremely strongly with my own thought process.

We were talking about the concept of working from home, and I mentioned to him that I would trust anyone in the office to not abuse that privilege. Keeping in mind that a lot of employees currently in the Entelect office are newish hires, he mentioned that trust has to be earned. This is a common saying, but one that I've never considered.

The true fact is that, when faced with an environment of mistrust, people are inclined to deceive their way through that situation, lest they be the target of more mistrust. 

The real answer is that trust should be assumed. One should approach every new individual with a high level of trusting behaviour. The reason for this is that trust begets trust. If you do not trust me, I can never give you a reason to trust me.

If, however, you do trust me, I can either break that trust or I can build upon it. This is, in my opinion, a much more direct way to judge character -- even if it does lead to your trust being broken more often, what do they always say in Software Development?

*Fail faster.*